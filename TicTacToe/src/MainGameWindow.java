import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;

public class MainGameWindow {

	protected Shell shell;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			MainGameWindow window = new MainGameWindow();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		
//			Zur Ermittlung der Displaygröße 
//		   final Monitor monitor = display.getPrimaryMonitor();
//		   final Rectangle rect;
//		 		   
//		   if (monitor != null) {
//		      rect = monitor.getClientArea();
//		   } else {
//		      // In case we cannot find the primary monitor get the entire display rectangle
//		      // Note that it may include the dimensions of multiple monitors. 
//		      rect = display.getBounds();
//		   }
//		   System.out.println("Monitor width=" + String.valueOf(rect.width));
//		   System.out.println("Monitor height=" + String.valueOf(rect.height));
//		   
//		shell.setMinimumSize(rect.width, rect.height); 
		
		
		shell.setFullScreen(true);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		
		Button btnClose = new Button(shell, SWT.CENTER);
		GridData gd_btnClose = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_btnClose.widthHint = 133;
		gd_btnClose.heightHint = 92;
		btnClose.setLayoutData(gd_btnClose);
		btnClose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.close();
			}
		});
		btnClose.setText("Close");
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GREEN));
		shell.setSize(719, 382);
		shell.setText("TicTacToe");
		shell.setLayout(new GridLayout(19, false));

	}

 
}
